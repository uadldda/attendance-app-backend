<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'cors'], function(){
    Route::apiResource('coordinators','CoordinatorController');//->middleware('auth:api');
    Route::apiResource('groups','GroupController');//->middleware('auth:api');
    Route::apiResource('students','StudentController');//->middleware('auth:api');
    Route::apiResource('teachers','TeacherController');//->middleware('auth:api');
    Route::apiResource('subjects','SubjectController');//->middleware('auth:api');
    Route::apiResource('registries','RegistryController');//->middleware('auth:api');
    Route::apiResource('registry_details','RegistryDetailController');//->middleware('auth:api');
    Route::apiResource('lessons','LessonController');//->middleware('auth:api');
    Route::get('teacher_class/{teacherid}', 'CardController@getTeacherClasses');//->middleware('auth:api');
    Route::apiResource('cards','CardController');//->middleware('auth:api');
    Route::get('code', 'HotpController@generateCode');//->middleware('auth:api');
    Route::post('code', 'HotpController@validateCode');//->middleware('auth:api');
    Route::post('login', 'UserController@login');//->middleware('auth:api');
    Route::post('details', 'UserController@details');//->middleware('auth:api');
    Route::post('register', 'UserController@register');//->middleware('auth:api');
    Route::post('xml_database', 'XmlUpload@storeToDatabase');//->middleware('auth:api');
    Route::get('student-group', 'StudentController@insertIntoPivotTable');
    Route::post('student-classes', 'StudentController@getStudentClasses');
    Route::post('create-registry', 'RegistryController@generateRegistry');
    Route::post('get-registries', 'RegistryController@getAllRegistriesFromTeacher');
    Route::post('create-registry-detail', 'RegistryDetailController@generateRegistryDetails');
    
});

