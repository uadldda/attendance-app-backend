<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $table = 'lessons';
    protected $fillable = [
        'id', 'teacher_id', 'group_id', 'subject_id'
    ];

    protected $primaryKey = 'id';
    public $incrementing = false;


    public function teacher(){
        return $this->belongsTo('App\Teacher');
    }
    public function group(){
        return $this->belongsTo('App\Group');
    }
    public function subject(){
        return $this->belongsTo('App\Subject');
    }
    public function card(){
        return $this->hasMany('App\Card');
    }
}

