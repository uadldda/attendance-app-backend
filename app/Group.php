<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';
    protected $fillable = [
        'id', 'groupsname'
    ];

    protected $primaryKey = 'id';
    public $incrementing = false;

    public function student(){
        return $this->belongsToMany('App\Student');
    }
    public function coordinator(){
        return $this->belongsTo('App\Coordinator');
    }
    public function lesson(){
        return $this->hasMany('App\Lesson');
    }
}
