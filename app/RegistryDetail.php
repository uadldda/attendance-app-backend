<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistryDetail extends Model
{
    protected $table = 'registry_details';
    protected $fillable = [
        'total_assis', 'registry_id', 'student_id'
    ];

    public function registry(){
        return $this->belongsTo('App\Registry');
    }
    public function student(){
        return $this->belongsTo('App\Student');
    }
}
