<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registry extends Model
{
    protected $table = 'registries';
    protected $fillable = [
        'date_time', 'subject', 'group', 'teacher', 'day', 'start_time', 'end_time' 
    ];
}
