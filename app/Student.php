<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    protected $fillable = [
        'id', 'name', 'email', 'role'
    ];

    protected $primaryKey = 'id';

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function group(){
        return $this->belongsToMany('App\Group');
    }
}   
