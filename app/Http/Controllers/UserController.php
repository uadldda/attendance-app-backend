<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;  
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\Teacher;
use App\Student;
use Validator;

class UserController extends Controller
{
    public $successStatus = 200;

    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            if(Teacher::where('email', '=', $user->email)->exists()){
                $user->uuid = DB::table('teachers')->where('email', $user->email)->first()->id;
                $user->role = "teacher";  
            } else if(Student::where('email', '=', $user->email)->exists()){
                $user->role = "student";
            }
            return response()->json(['success' => $success, 'user' => $user], $this->successStatus); 
        }
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        if (Teacher::where('email', '=', $request->email)->exists() || Student::where('email', '=', $request->email)->exists()) {
            if (User::where('email', '=', $request->email)->exists()){
                return response()->json(['error'=>"Está cuenta ya esta registrada"], 403);
            }
            $input = $request->all(); 
            $input['password'] = bcrypt($input['password']); 
            $user = User::create($input); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            $success['name'] =  $user->name;
            return response()->json(['success'=>$success], $this-> successStatus);
         }else{
            return response()->json(['error' => "Tu correo no es institucional"], 401);
         }
    }
}
