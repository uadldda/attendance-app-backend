<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Card;
use App\Lesson;
use App\Teacher;
use App\Period;

class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cards = Card::all();
        foreach($cards as $card){            
            $card->lesson->teacher;
            $card->lesson->subject;
            $card->lesson->group;
            $card->period;
            /*dd($cards);
            foreach($cards as $card)
            {
                $period=App\Period::find($card->period_id);
                dd($period);
            }*/
            
        }
        return response()->json($cards, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $card = Card::create($request->all());
        return $card;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Card $card)
    {
        $cards = Card::find($card);
        foreach($cards as $card){            
            $card->lesson->teacher->name;
            $card->lesson->subject->name;
            $card->lesson->group->groupsname;
            $card->period;
        }
        return response()->json($cards, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Card $card)
    {
        $cards = $card;
        $cards->fill($request->all()->save());
        return $cards;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Card $card)
    {
        $cards = $card;
        $cards->delete();
        return $cards;
    }

    /**
     * Get teacher's id and passes the class attributes
     */
    public function getTeacherClasses($teacher){
        $lessons = Lesson::where('teacher_id', $teacher)->get();
        foreach($lessons as $lesson){
            $cards = $lesson->card;
            foreach($cards as $card)
            {
                $period=Period::find($card->period_id);
                $card->period=$period;
            }
            $lesson->group;
            $lesson->subject;
        }
        return response()->json($lessons, 200);
    }
}
