<?php

namespace App\Http\Controllers;

use App\Registry;
use Carbon\Carbon;
use App\Subject;
use Illuminate\Http\Request;

class RegistryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registry = Registry::all();
        return $registry;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $registry = Registry::create($request->all());
        return $registry;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Registry  $registry
     * @return \Illuminate\Http\Response
     */
    public function show(Registry $registry)
    {
        $registries = Career::find($registry);
        return $registries;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Registry  $registry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Registry $registry)
    {
        $registries = $registry;
        $registries->fill($request->all())->save();
        return $registries;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Registry  $registry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Registry $registry)
    {
        $registries = $registry;
        $registries->delete();
        return $registries; 
    }

    public function generateRegistry(Request $request){
        $registry = new Registry;
        $registry->subject = $request->subject;
        $registry->group = $request->group;
        $registry->teacher = $request->teacher;
        $registry->day = $request->day;
        $registry->start_time = $request->start_time;
        $registry->end_time = $request->end_time;
        $registry->date_time = Carbon::now('America/Mexico_City')->addHour();

        $registry->save();
        return response()->json($registry);
    }

    public function getAllRegistriesFromTeacher(Request $request){
        $registries = Registry::where('subject', $request->name)->get();
        return $registries;
    }
}
