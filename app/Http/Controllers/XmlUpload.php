<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Subject;
use App\Group;
use App\Period;
use App\Lesson;
use App\Card;
use App\Teacher;

class XmlUpload extends Controller
{   
	public function storeToDatabase(Request $file){

    $default_password = "uad2019";
    $role_teacher = "teacher";
    $default = 1;

    $path = Storage::putFile('migraciones', $file->migracion);
    $contents = Storage::get($path);
    $xml = new \SimpleXMLElement($contents);

    foreach($xml->xpath("/timetable/teachers/teacher") as $teacher){
      $db_teacher = new Teacher();
      $db_teacher->id = (string)$teacher->attributes()->id;
      $db_teacher->name = (string)$teacher->attributes()->name;
      $db_teacher->email = (string)$teacher->attributes()->email;
      $db_teacher->role = $role_teacher;
      if(empty($db_teacher->email)){
        $db_teacher->email = "empty " . $default;
        $default += 1;
      }
      $db_teacher->save();
      // if(!empty($db_user->email)){
      //   $db_user->password = preg_split("/\@\w+\.\w+.\w+/", $db_user->email, PREG_SPLIT_DELIM_CAPTURE);
      //   $db_user->password = $db_user->password[0];
      //   $db_user->password = hash("sha256", $db_user->password);
      // }else{
      //   $db_user->email = "empty " . $default;
      //   $db_user->password = $default_password;
      //   $db_user->password = hash("sha256", $db_user->password);
      //   $default += 1; 
      // }
    }
    foreach($xml->xpath("/timetable/subjects/subject") as $subject){
      $db_subject = new Subject();
      $db_subject->id = (string)$subject->attributes()->id;
      $db_subject->name = (string)$subject->attributes()->name;
      $db_subject->save();
    }
    foreach($xml->xpath("/timetable/classes/class") as $group){
      $db_group = new Group;
      $db_group->id = (string)$group->attributes()->id;
      $db_group->groupsname = (string)$group->attributes()->name;
      //$db_group->coordinator_id = 
      $db_group->save();
    }
    foreach($xml->xpath("/timetable/periods/period") as $period){
      $db_period = new Period;
      $db_period->id = (string)$period->attributes()->name;
      $db_period->start_time = (string)$period->attributes()->starttime;
      $db_period->end_time = (string)$period->attributes()->endtime;
      $db_period->save();
    }
    foreach($xml->xpath("/timetable/lessons/lesson") as $lesson){
      $db_lesson = new Lesson();
      $db_lesson->id = (string)$lesson->attributes()->id;
      $db_lesson->group_id = (string)$lesson->attributes()->classids;
      $db_lesson->subject_id = (string)$lesson->attributes()->subjectid;
      $db_lesson->teacher_id = (string)$lesson->attributes()->teacherids;
      $db_lesson->save();
    }
    foreach($xml->xpath("/timetable/cards/card") as $card){
      $db_card = new Card();
      $db_card->period_id = (string)$card->attributes()->period;
      $db_card->lesson_id = (string)$card->attributes()->lessonid;
      if((string)$card->attributes()->days == "100000")
      {$db_card->day = "Lunes";}
      else if((string)$card->attributes()->days == "010000")
      {$db_card->day = "Martes";}
      else if((string)$card->attributes()->days == "001000")
      {$db_card->day = "Miércoles";}
      else if((string)$card->attributes()->days == "000100")
      {$db_card->day = "Jueves";}
      else if((string)$card->attributes()->days == "000010")
      {$db_card->day = "Viernes";}
      else if((string)$card->attributes()->days == "000001")
      {$db_card->day = "Sábado";}
      $db_card->save();
    }
    return "Database tables were updated";
	}
}