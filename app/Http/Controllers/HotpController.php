<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Providers\HOTP;
use App\Providers\HOTPResult;

class HotpController extends Controller
{  
  public function generateCode(){
    $key = 'UAD2019A';
    $window = 15;
    $min = -1;
    $max = 1;
    $codeArray = [];

    $results = HOTP::generateByTimeWindow($key, $window, $min, $max);
    foreach ($results as $key => $result) {
      array_push($codeArray, $result->toHOTP(6));
    }
    return $codeArray;
  }

  public function validateCode(Request $studentCode){
    $result = new HotpController();
    $result = $result->generateCode();
    $response_success = ["You are registered"];
    $response_success = json_encode($response_success);
    $response_fail = ["Please try again"];
    $response_fail = json_encode($response_fail);

    if(in_array($studentCode["studentCode"], $result)){
      return $response_success;  
    }else{
      return $response_fail;
    }
  }
}