<?php

namespace App\Http\Controllers;

use App\Student;
use App\Group;
use App\Period;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $student = Student::all();
        return $student;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student = Student::create($request->all());
        return $student;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        $students = Student::find($student);
        return $students;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $students = $student;
        $students->fill($request->all())->save();
        return $students;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $students = $student;
        $students->delete();
        return $students;
    }

    public function insertIntoPivotTable(){
        $student = new Student;
        $student->name = 'Kelvin Mandujano Toscano';
        $student->email = 'ldda17c.kmandujano@uartesdigitales.edu.mx';
        $student->role = 'student';
        $student->save();

        $group = Group::find('63DF292B1716443A');
        
        $student->group()->attach($group);
        return 'Success';
    }

    public function getStudentClasses(Request $request){
        $student = Student::where('email', $request->email)->get();
        $classes = Student::where('id', $student[0]->id)->with('group.lesson')->get();
        
        foreach($classes[0]->group[0]->lesson as $class){
            $class->teacher;
            $class->subject;
            $cards = $class->card;
            foreach($cards as $card){
                $period=Period::find($card->period_id);
                $card->period=$period;
            }
        }
        return response()->json($classes);
    }
}
