<?php

namespace App\Http\Controllers;

use App\RegistryDetail;
use Illuminate\Http\Request;

class RegistryDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registryDetail = RegistryDetail::all();
        return $registryDetail;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $registryDetail = Career::create($request->all());
        return $registryDetail;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RegistryDetail  $registryDetail
     * @return \Illuminate\Http\Response
     */
    public function show(RegistryDetail $registryDetail)
    {
        $registryDetails = Career::find($registryDetail);
        return $registryDetails;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegistryDetail  $registryDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegistryDetail $registryDetail)
    {
        $registryDetails = $registryDetail;
        $registryDetails->fill($request->all())->save();
        return $registryDetails;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegistryDetail  $registryDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegistryDetail $registryDetail)
    {
        $registryDetails = $registryDetail;
        $registryDetails->delete();
        return $registryDetails;
    }

    public function generateRegistryDetails(Request $request){
        return $request;
    }
}
