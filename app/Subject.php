<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subjects';
    
    protected $fillable = [
        'id', 'name'
    ];
    protected $primaryKey = 'id';
    public $incrementing = false;

    public function lesson(){
        return $this->hasMany('App\Lesson');
    }
}