<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinator extends Model
{
    protected $table = 'coordinators';
    protected $fillable = [
        'name', 'career_name'
    ];
}
