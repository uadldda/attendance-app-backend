<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $table = 'cards';
    protected $fillable = [
        'period_id', 'lesson_id'
    ];

    protected $primaryKey = 'lesson_id';
    public $incrementing = false;

    

    public function lesson(){
        return $this->belongsTo('App\Lesson');
    }
    public function period(){
        return $this->belongsTo('App\Period');
    }
}