<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $table = 'teachers';
    protected $fillable = [
        'id', 'name', 'email', 'role'
    ];   

    protected $primaryKey = 'id';
    public $incrementing = false;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function lesson(){
        return $this->hasMany('App\Lesson');
    }
}
